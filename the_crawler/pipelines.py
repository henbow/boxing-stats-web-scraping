# -*- coding: utf-8 -*-

"""
Crawler Pipelines
"""

__author__ = 'hendro'

from the_crawler.libraries.Models import Events, EventFights, Fighters, Fights, FightTotalStats, FightTotalPerRoundStats
from the_crawler.libraries.helpers import type_parser, performance_bonus_type
from scrapy.exceptions import DropItem
from BeautifulSoup import BeautifulSoup
from sqlobject import AND

import re, json, os, datetime


class ProcessHTML(object):
    def process_item(self, item, spider):
        spider.log("Spider name: "+spider.name)
        try:
            if spider.name == 'FightMetricsPerRoundEngine':
                # item = self._process_fight_detail_per_round_data(item)
                return item
            else:
                type = type_parser(item['url'])
                if type == 'event-details':
                    item = self._process_event_data(item)

                    if item['event_name'] == '':
                        raise DropItem("No event name!")
                    else:
                        return item
                elif type == 'fighter-details':
                    item = self._process_fighter_data(item)

                    if item['fullname'] == '':
                        raise DropItem("No fighter name!")
                    else:
                        return item
                else:
                    item = self._process_fight_detail_data(item)

                    if item['event'] == '' and item['fighter_name'] and item['opponent_name']:
                        raise DropItem("No fight detail!")
                    else:
                        return item
        except KeyError, e:
            raise DropItem("Key %s not found" % e)

    def _process_per_round_data(self, item):
        total_rounds = int(self._sanitize_html(item['pr_total_round'][0]).strip())

        for i in range(1, total_rounds):
            item['pr_fighters'][i] = self._sanitize_html(item['pr_fighters'][i]).strip()
            item['pr_win_or_lose'][i] = self._sanitize_html(item['pr_win_or_lose'][i]).strip()
            item['pr_kd'][i] = self._sanitize_html(item['pr_kd'][i]).strip()
            item['pr_sig_str'][i] = self._sanitize_html(item['pr_sig_str'][i]).strip()
            item['pr_sig_str_percent'][i] = self._sanitize_html(item['pr_sig_str_percent'][i]).strip()
            item['pr_total_str'][i] = self._sanitize_html(item['pr_total_str'][i]).strip()
            item['pr_td'][i] = self._sanitize_html(item['pr_td'][i]).strip()
            item['pr_td_percent'][i] = self._sanitize_html(item['pr_td_percent'][i]).strip()
            item['pr_sub_att'][i] = self._sanitize_html(item['pr_sub_att'][i]).strip()
            item['pr_the_pass'][i] = self._sanitize_html(item['pr_the_pass'][i]).strip()
            item['pr_rev'][i] = self._sanitize_html(item['pr_rev'][i]).strip()

            # fighter_1 = list(Fighters.select(Fighters.q.name==item['pr_fighters'][i]))
            # if fighter_1 :
            #     item['pr_fighter_ids'].append(int(fighter_1[i].id))

            item['pr_sig_sig_str'][i] = self._sanitize_html(item['pr_sig_sig_str'][i]).strip()
            item['pr_sig_sig_str_percent'][i] = self._sanitize_html(item['pr_sig_sig_str_percent'][i]).strip()
            item['pr_sig_head'][i] = self._sanitize_html(item['pr_sig_head'][i]).strip()
            item['pr_sig_body'][i] = self._sanitize_html(item['pr_sig_body'][i]).strip()
            item['pr_sig_leg'][i] = self._sanitize_html(item['pr_sig_leg'][i]).strip()
            item['pr_sig_distance'][i] = self._sanitize_html(item['pr_sig_distance'][i]).strip()
            item['pr_sig_clinch'][i] = self._sanitize_html(item['pr_sig_clinch'][i]).strip()
            item['pr_sig_ground'][i] = self._sanitize_html(item['pr_sig_ground'][i]).strip()

        return item

    def _process_event_data(self, item):
        item['event_name'] = self._sanitize_html(item['event_name']).strip()
        item['location'] = self._sanitize_html(item['location']).strip()

        attendances = self._sanitize_html(item['attendances']).strip()
        item['attendances'] = int(attendances.replace(',', '')) if attendances else 0

        event_dt = self._sanitize_html(item['event_date']).strip()
        dt = datetime.datetime.strptime(event_dt, '%B %d, %Y')
        item['event_date'] = '{0}-{1}-{2}'.format(dt.year, dt.month, dt.day)

        item['total_fights'] = int(item['total_fights'].replace('.0', ''))

        return item

    def _process_fighter_data(self, item):
        item['fullname'] = self._sanitize_html(item['fullname']).strip()
        item['height'] = self._sanitize_html(item['height']).strip()
        item['weight'] = self._sanitize_html(item['weight']).strip()
        item['reach'] = self._sanitize_html(item['reach']).strip()
        item['stance'] = self._sanitize_html(item['stance']).strip()
        item['dob'] = self._sanitize_html(item['dob']).strip()

        if item['dob'] == '--':
            item['dob'] = None
        else:
            dob_dt = self._sanitize_html(item['dob']).strip()
            dt = datetime.datetime.strptime(dob_dt, '%b %d, %Y')
            item['dob'] = '{0}-{1}-{2}'.format(dt.year, dt.month, dt.day)

        return item

    def _process_fight_detail_data(self, item):
        item['event'] = self._sanitize_html(item['event']).strip()
        item['method'] = self._sanitize_html(item['method']).strip()
        item['total_rounds'] = self._sanitize_html(item['round']).strip()
        item['time'] = self._sanitize_html(item['time']).strip()
        item['time_format'] = self._sanitize_html(item['time_format']).strip()
        item['referee'] = self._sanitize_html(item['referee']).strip()
        item['details'] = self._sanitize_html(item['details']).strip()
        item['weight_class'] = self._sanitize_html(item['weight_class']).strip()

        item['fighters'][0] = self._sanitize_html(item['fighters'][0]).strip()
        item['fighters'][1] = self._sanitize_html(item['fighters'][1]).strip()
        item['win_or_lose'][0] = self._sanitize_html(item['win_or_lose'][0]).strip()
        item['win_or_lose'][1] = self._sanitize_html(item['win_or_lose'][1]).strip()
        item['kd'][0] = self._sanitize_html(item['kd'][0]).strip()
        item['kd'][1] = self._sanitize_html(item['kd'][1]).strip()
        item['sig_str'][0] = self._sanitize_html(item['sig_str'][0]).strip()
        item['sig_str'][1] = self._sanitize_html(item['sig_str'][1]).strip()
        item['sig_str_percent'][0] = self._sanitize_html(item['sig_str_percent'][0]).strip()
        item['sig_str_percent'][1] = self._sanitize_html(item['sig_str_percent'][1]).strip()
        item['total_str'][0] = self._sanitize_html(item['total_str'][0]).strip()
        item['total_str'][1] = self._sanitize_html(item['total_str'][1]).strip()
        item['td'][0] = self._sanitize_html(item['td'][0]).strip()
        item['td'][1] = self._sanitize_html(item['td'][1]).strip()
        item['td_percent'][0] = self._sanitize_html(item['td_percent'][0]).strip()
        item['td_percent'][1] = self._sanitize_html(item['td_percent'][1]).strip()
        item['sub_att'][0] = self._sanitize_html(item['sub_att'][0]).strip()
        item['sub_att'][1] = self._sanitize_html(item['sub_att'][1]).strip()
        item['the_pass'][0] = self._sanitize_html(item['the_pass'][0]).strip()
        item['the_pass'][1] = self._sanitize_html(item['the_pass'][1]).strip()
        item['rev'][0] = self._sanitize_html(item['rev'][0]).strip()
        item['rev'][1] = self._sanitize_html(item['rev'][1]).strip()

        event = list(Events.select(Events.q.eventName==item['event']))
        item['event_id'] = int(event[0].id) if event else None

        fighter_1 = list(Fighters.select(Fighters.q.name==item['fighters'][0]))
        fighter_2 = list(Fighters.select(Fighters.q.name==item['fighters'][1]))
        if fighter_1 and fighter_2:
            item['fighter_ids'].append(int(fighter_1[0].id))
            item['fighter_ids'].append(int(fighter_2[0].id))

        item['sig_sig_str'][0] = self._sanitize_html(item['sig_sig_str'][0]).strip()
        item['sig_sig_str'][1] = self._sanitize_html(item['sig_sig_str'][1]).strip()
        item['sig_sig_str_percent'][0] = self._sanitize_html(item['sig_sig_str_percent'][0]).strip()
        item['sig_sig_str_percent'][1] = self._sanitize_html(item['sig_sig_str_percent'][1]).strip()
        item['sig_head'][0] = self._sanitize_html(item['sig_head'][0]).strip()
        item['sig_head'][1] = self._sanitize_html(item['sig_head'][1]).strip()
        item['sig_body'][0] = self._sanitize_html(item['sig_body'][0]).strip()
        item['sig_body'][1] = self._sanitize_html(item['sig_body'][1]).strip()
        item['sig_leg'][0] = self._sanitize_html(item['sig_leg'][0]).strip()
        item['sig_leg'][1] = self._sanitize_html(item['sig_leg'][1]).strip()
        item['sig_distance'][0] = self._sanitize_html(item['sig_distance'][0]).strip()
        item['sig_distance'][1] = self._sanitize_html(item['sig_distance'][1]).strip()
        item['sig_clinch'][0] = self._sanitize_html(item['sig_clinch'][0]).strip()
        item['sig_clinch'][1] = self._sanitize_html(item['sig_clinch'][1]).strip()
        item['sig_ground'][0] = self._sanitize_html(item['sig_ground'][0]).strip()
        item['sig_ground'][1] = self._sanitize_html(item['sig_ground'][1]).strip()

        return item

    def _process_fight_detail_per_round_data(self, item):
        item['pr_fighters'][0] = self._sanitize_html(item['pr_fighters'][0]).strip()
        item['pr_fighters'][1] = self._sanitize_html(item['pr_fighters'][1]).strip()
        item['pr_win_or_lose'][0] = self._sanitize_html(item['pr_win_or_lose'][0]).strip()
        item['pr_win_or_lose'][1] = self._sanitize_html(item['pr_win_or_lose'][1]).strip()
        item['pr_kd'][0] = self._sanitize_html(item['pr_kd'][0]).strip()
        item['pr_kd'][1] = self._sanitize_html(item['pr_kd'][1]).strip()
        item['pr_sig_str'][0] = self._sanitize_html(item['pr_sig_str'][0]).strip()
        item['pr_sig_str'][1] = self._sanitize_html(item['pr_sig_str'][1]).strip()
        item['pr_sig_str_percent'][0] = self._sanitize_html(item['pr_sig_str_percent'][0]).strip()
        item['pr_sig_str_percent'][1] = self._sanitize_html(item['pr_sig_str_percent'][1]).strip()
        item['pr_total_str'][0] = self._sanitize_html(item['pr_total_str'][0]).strip()
        item['pr_total_str'][1] = self._sanitize_html(item['pr_total_str'][1]).strip()
        item['pr_td'][0] = self._sanitize_html(item['pr_td'][0]).strip()
        item['pr_td'][1] = self._sanitize_html(item['pr_td'][1]).strip()
        item['pr_td_percent'][0] = self._sanitize_html(item['pr_td_percent'][0]).strip()
        item['pr_td_percent'][1] = self._sanitize_html(item['pr_td_percent'][1]).strip()
        item['pr_sub_att'][0] = self._sanitize_html(item['pr_sub_att'][0]).strip()
        item['pr_sub_att'][1] = self._sanitize_html(item['pr_sub_att'][1]).strip()
        item['pr_fd_pass'][0] = self._sanitize_html(item['pr_fd_pass'][0]).strip()
        item['pr_fd_pass'][1] = self._sanitize_html(item['pr_fd_pass'][1]).strip()
        item['pr_rev'][0] = self._sanitize_html(item['pr_rev'][0]).strip()
        item['pr_rev'][1] = self._sanitize_html(item['pr_rev'][1]).strip()

        fighter_1 = list(Fighters.select(Fighters.q.name==item['pr_fighters'][0]))
        fighter_2 = list(Fighters.select(Fighters.q.name==item['pr_fighters'][1]))
        if fighter_1 and fighter_2:
            item['pr_fighter_ids'].append(int(fighter_1[0].id))
            item['pr_fighter_ids'].append(int(fighter_2[0].id))

        item['pr_sig_sig_str'][0] = self._sanitize_html(item['pr_sig_sig_str'][0]).strip()
        item['pr_sig_sig_str'][1] = self._sanitize_html(item['pr_sig_sig_str'][1]).strip()
        item['pr_sig_sig_str_percent'][0] = self._sanitize_html(item['pr_sig_sig_str_percent'][0]).strip()
        item['pr_sig_sig_str_percent'][1] = self._sanitize_html(item['pr_sig_sig_str_percent'][1]).strip()
        item['pr_sig_head'][0] = self._sanitize_html(item['pr_sig_head'][0]).strip()
        item['pr_sig_head'][1] = self._sanitize_html(item['pr_sig_head'][1]).strip()
        item['pr_sig_body'][0] = self._sanitize_html(item['pr_sig_body'][0]).strip()
        item['pr_sig_body'][1] = self._sanitize_html(item['pr_sig_body'][1]).strip()
        item['pr_sig_leg'][0] = self._sanitize_html(item['pr_sig_leg'][0]).strip()
        item['pr_sig_leg'][1] = self._sanitize_html(item['pr_sig_leg'][1]).strip()
        item['pr_sig_distance'][0] = self._sanitize_html(item['pr_sig_distance'][0]).strip()
        item['pr_sig_distance'][1] = self._sanitize_html(item['pr_sig_distance'][1]).strip()
        item['pr_sig_clinch'][0] = self._sanitize_html(item['pr_sig_clinch'][0]).strip()
        item['pr_sig_clinch'][1] = self._sanitize_html(item['pr_sig_clinch'][1]).strip()
        item['pr_sig_ground'][0] = self._sanitize_html(item['pr_sig_ground'][0]).strip()
        item['pr_sig_ground'][1] = self._sanitize_html(item['pr_sig_ground'][1]).strip()

        return item

    def _sanitize_html(self, html):
        soup = BeautifulSoup(html)

        for tag in soup.findAll(True):
            tag.hidden = True

        pattern1 = re.compile(r"<!\s*--(.*?)(--\s*\>)", re.DOTALL | re.MULTILINE | re.IGNORECASE)
        pattern2 = re.compile(r"[\n\s]+", re.DOTALL | re.MULTILINE | re.IGNORECASE)
        content = pattern1.sub('', soup.renderContents())

        return pattern2.sub(' ', content)


class CheckDuplicateData(object):
    def __init__(self, cfg_path):
        self.cfg_path = cfg_path[0]

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('CRAWLED_CONTENT_PATH'))

    def process_item(self, item, spider):
        try:
            type = type_parser(item['url'])
            if type == 'event-details':
                event_count = Events.select(Events.q.eventName==item['event_name']).count()
                if event_count > 0:
                    raise DropItem("Found duplicate event! Drop it!")
            elif type == 'fighter-details':
                fighter_count = Fighters.select(Fighters.q.name==item['fullname']).count()
                if fighter_count > 0:
                    raise DropItem("Found duplicate fighter! Drop it!")
            else:
                pass

            return item
        except KeyError:
            raise DropItem("KeyError: Failed process data!")
        except TypeError:
            raise DropItem("TypeError: Failed process data!")


class SaveResults(object):
    def process_item(self, item, spider):
        type = type_parser(item['url'])
        if type == 'event-details':
            event = Events(
                eventName = item['event_name'],
                eventDate = item['event_date'],
                location = item['location'],
                attendance = item['attendances'],
                totalFights = item['total_fights'],
                createdAt = datetime.datetime.now(),
                updatedAt = datetime.datetime.now()
            )

            for url, num in item['fights'].iteritems():
                EventFights(
                    eventID = event.id,
                    url = url,
                    fightNum = num
                )
        elif type == 'fighter-details':
            Fighters(
                name = item['fullname'],
                height = item['height'],
                weight = item['weight'],
                reach = item['reach'],
                stance = item['stance'],
                dob = item['dob'],
                createdAt = datetime.datetime.now()
            )
        else:
            if item['event_id'] and len(item['fighter_ids']) > 0:
                fight_detail = Fights.select(AND(Fights.eventID==int(item['event_id'])))
                if fight_detail.count() == 0:
                    fight_num = list(EventFights.select(EventFights.q.url==item['url']))
                    if len(fight_num) > 0:
                        fight = Fights(
                            eventID = int(item['event_id']),
                            weightClass = item['weight_class'],
                            currentFightNum = fight_num[0].fightNum,
                            method = item['method'],
                            round = int(item['round']),
                            time = item['time'],
                            timeFormat = item['time_format'],
                            referee = item['referee'],
                            details = item['details'],
                            createdAt = datetime.datetime.now()
                        )
                    else:
                        raise DropItem("Data is not complete!")
                else:
                    fight = list(fight_detail)[0]

                for i in [0, 1]:
                    fight_detail_stats_count = FightTotalStats.select(AND(FightTotalStats.fightDetailID==fight.id, FightTotalStats.fighterID==item['fighter_ids'][i])).count()
                    if fight_detail_stats_count > 0:
                        raise DropItem("Found duplicate fight detail stats! Drop it!")
                    FightTotalStats(
                        fightDetailID = int(fight.id),
                        fighterID = int(item['fighter_ids'][i]),
                        winOrLose = item['win_or_lose'][i],
                        totalKd = int(item['kd'][i]),
                        totalSigStr = item['sig_str'][i],
                        totalSigStrPercent = item['sig_str_percent'][i],
                        totalTotalStr = item['total_str'][i],
                        totalTd = item['td'][i],
                        totalTdPercent = item['td_percent'][i],
                        totalSubAtt = int(item['sub_att'][i]),
                        totalPass = int(item['the_pass'][i]),
                        totalRev = int(item['rev'][i]),
                        sigSigStr = item['sig_sig_str'][i],
                        sigSigStrPercent = item['sig_sig_str_percent'][i],
                        sigHead = item['sig_head'][i],
                        sigBody = item['sig_body'][i],
                        sigLeg = item['sig_leg'][i],
                        sigDistance = item['sig_distance'][i],
                        sigClinch = item['sig_clinch'][i],
                        sigGround = item['sig_ground'][i]
                    )

        return item


class SaveResultsJSON(object):
    def __init__(self, cfg_path):
        self.cfg_path = cfg_path[0]

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('CRAWLED_CONTENT_PATH'))

    def process_item(self, item, spider):
        if not os.path.exists('%s/events' % self.cfg_path):
            os.makedirs('%s/events' % self.cfg_path)
        file_name = item['event_name'].lower().replace(' ', '_').replace(':', '_')
        file_content = open('%s/events/%s.json' % (self.cfg_path, file_name), 'wb')
        line = json.dumps(dict(item))
        file_content.write(line)

        return item

