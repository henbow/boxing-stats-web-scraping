from sqlobject import SQLObject
from the_crawler.libraries.connection import conn

__connection__ = conn


class Events(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'events'


class EventFights(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'event_fight_urls'


class Fights(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'fight_details'


class FightTotalStats(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'fight_detail_total_stats'


class FightTotalPerRoundStats(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'fight_detail_per_round_stats'


class Fighters(SQLObject):
    class sqlmeta:
        fromDatabase = True
        table = 'fighters'
