from scrapy.item import Item, Field


class EventAttributes(Item):
    event_name = Field()
    event_date = Field()
    location = Field()
    attendances = Field()
    total_fights = Field()
    fights = Field()
    url = Field()
    referer = Field()
    size = Field()
    hash = Field()
    newcookies = Field()


class FightDetailAttributes(Item):
    # Main
    detail_id = Field()
    event = Field()
    event_id = Field()
    win_or_lose = Field()
    weight_class = Field()
    method = Field()
    round = Field()
    time = Field()
    time_format = Field()
    referee = Field()
    details = Field()
    total_rounds = Field()
    number_of_fight = Field()
    title_fight = Field()
    performance_bonus = Field()
    performance_bonus_type = Field()

    # Stats
    fighters = Field()
    fighter_ids = Field()
    kd = Field()
    sig_str = Field()
    sig_str_percent = Field()
    total_str = Field()
    td = Field()
    td_percent = Field()
    sub_att = Field()
    the_pass = Field()
    rev = Field()

    # Strike
    sig_sig_str = Field()
    sig_sig_str_percent = Field()
    sig_head = Field()
    sig_body = Field()
    sig_leg = Field()
    sig_distance = Field()
    sig_clinch = Field()
    sig_ground = Field()

    # Misc
    url = Field()
    referer = Field()
    size = Field()
    hash = Field()
    newcookies = Field()


class FightPerRoundDetailAttribute(Item):
    # Stats
    pr_total_round = Field()
    pr_round = Field()
    pr_fighters = Field()
    pr_fighter_ids = Field()
    pr_kd = Field()
    pr_sig_str = Field()
    pr_sig_str_percent = Field()
    pr_total_str = Field()
    pr_td = Field()
    pr_td_percent = Field()
    pr_sub_att = Field()
    pr_the_pass = Field()
    pr_rev = Field()

    # Strike
    pr_sig_sig_str = Field()
    pr_sig_sig_str_percent = Field()
    pr_sig_head = Field()
    pr_sig_body = Field()
    pr_sig_leg = Field()
    pr_sig_distance = Field()
    pr_sig_clinch = Field()
    pr_sig_ground = Field()

    # Misc
    url = Field()
    referer = Field()
    size = Field()
    hash = Field()
    newcookies = Field()


class FighterAttributes(Item):
    fullname = Field()
    height = Field()
    weight = Field()
    reach = Field()
    stance = Field()
    dob = Field()
    url = Field()
    referer = Field()
    size = Field()
    hash = Field()
    newcookies = Field()
