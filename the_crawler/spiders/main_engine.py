from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.http import Request, Response
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor

from the_crawler.items import EventAttributes
from the_crawler.items import FightDetailAttributes
from the_crawler.items import FightPerRoundDetailAttribute
from the_crawler.items import FighterAttributes
from the_crawler.settings import SITE_CONFIG_PATH
from the_crawler.libraries.helpers import parse_config, type_parser, performance_bonus_type

class MainBotEngine(Spider):

    name = 'FightMetricsEngine'

    def __init__(self, **kw):
        super(MainBotEngine, self).__init__(**kw)

        self.config_path = '%s/fightmetrics.txt' % SITE_CONFIG_PATH
        self.config_items = parse_config(self.config_path)

        self.url = self.config_items['start_url']

        try:
            self.link_extractor = LxmlLinkExtractor(restrict_xpaths=self.config_items['event_crawl_area'], unique=True)
        except KeyError:
            self.link_extractor = LxmlLinkExtractor(unique=True)

        self.cookies_seen = set()

    def start_requests(self):
        return [Request(self.url, callback=self.parse)]

    def parse(self, response):
        page = self._get_item(response)

        r = [page]
        r.extend(self._extract_requests(response))

        return r

    def _extract_requests(self, response):
        r = []
        if isinstance(response, Response):
            links = self.link_extractor.extract_links(response)
            r.extend(Request(x.url, callback=self.parse) for x in links)
        return r

    def _get_item(self, response):
        if isinstance(response, Response):
            type = type_parser(response.url)
            if type == 'event-details':
                return self._get_event_item(response)
            elif type == 'fighter-details':
                return self._get_fighter_item(response)
            else:
                return self._get_fight_item(response)

    def _get_event_item(self, response):
        self.log("Processing Event data")

        item = EventAttributes(
            url=response.url,
            size=str(len(response.body)),
            referer=response.request.headers.get('Referer')
        )
        self._set_event_data(item, response)
        self._set_new_cookies(item, response)

        return item

    def _get_fighter_item(self, response):
        self.log("Processing Fighter data")

        item = FighterAttributes(
            url=response.url,
            size=str(len(response.body)),
            referer=response.request.headers.get('Referer')
        )

        self._set_fighter_data(item, response)
        self._set_new_cookies(item, response)

        return item

    def _get_fight_item(self, response):
        self.log("Processing Fight details data")

        item = FightDetailAttributes(
            url=response.url,
            size=str(len(response.body)),
            referer=response.request.headers.get('Referer')
        )
        item_per_round = FightPerRoundDetailAttribute()

        self._set_fight_data(item, response)
        self._set_fight_stats_per_round_data(item_per_round, response)
        self._set_new_cookies(item, response)

        return item

    def _set_event_data(self, page, response):
        event_name = Selector(response).xpath(self.config_items['event_name']).extract()
        event_date = Selector(response).xpath(self.config_items['event_date']).extract()
        event_location = Selector(response).xpath(self.config_items['event_location']).extract()
        event_attendance = Selector(response).xpath(self.config_items['event_attendance']).extract()
        event_total_fights = Selector(response).xpath(self.config_items['event_total_fights']).extract()
        event_fights = Selector(response).xpath(self.config_items['event_fights']).extract()

        if event_name:
            page['event_name'] = event_name[0]
        if event_date:
            page['event_date'] = event_date[1]
        if event_location:
            page['location'] = event_location[1]
        if event_attendance:
            page['attendances'] = event_attendance[1]
        else:
            page['attendances'] = 0
        if event_total_fights:
            page['total_fights'] = event_total_fights[0]
        if len(event_fights) > 0:
            page['fights'] = {}
            i = 1
            for url in event_fights:
                page['fights'][url] = i
                i = i + 1

    def _set_fighter_data(self, page, response):
        name = Selector(response).xpath(self.config_items['fighter_name']).extract()
        height = Selector(response).xpath(self.config_items['fighter_height']).extract()
        weight = Selector(response).xpath(self.config_items['fighter_weight']).extract()
        reach = Selector(response).xpath(self.config_items['fighter_reach']).extract()
        stance = Selector(response).xpath(self.config_items['fighter_stance']).extract()
        dob = Selector(response).xpath(self.config_items['fighter_dob']).extract()

        if name:
            page['fullname'] = name[0]
        if height:
            page['height'] = height[1]
        if weight:
            page['weight'] = weight[1]
        if reach:
            page['reach'] = reach[1]
        if stance:
            page['stance'] = stance[1]
        if dob:
            page['dob'] = dob[1]

    def _set_fight_data(self, page, response):
        event = Selector(response).xpath(self.config_items['fight_event']).extract()
        weight_class = Selector(response).xpath(self.config_items['fight_weight_class']).extract()
        method = Selector(response).xpath(self.config_items['fight_method']).extract()
        round = Selector(response).xpath(self.config_items['fight_round']).extract()
        time = Selector(response).xpath(self.config_items['fight_time']).extract()
        time_format = Selector(response).xpath(self.config_items['fight_time_format']).extract()
        referee = Selector(response).xpath(self.config_items['fight_referee']).extract()
        details = Selector(response).xpath(self.config_items['fight_details']).extract()

        if event:
            page['event'] = " ".join(event).strip()
        if weight_class:
            page['weight_class'] = " ".join(weight_class).strip()
        if method:
            page['method'] = " ".join(method).strip()
        if round:
            page['round'] = " ".join(round).strip()
        if time:
            page['time'] = " ".join(time).strip()
        if time_format:
            page['time_format'] = " ".join(time_format).strip()
        if referee:
            page['referee'] = " ".join(referee).strip()
        if details:
            page['details'] = " ".join(details).replace('Details:', '').strip()

        # self._set_fight_stats_data(page, response)

    def _set_fight_stats_data(self, page, response):
        fighters = Selector(response).xpath(self.config_items['fd_fighter']).extract()
        win_or_lose = Selector(response).xpath(self.config_items['fd_win_or_lose']).extract()
        kd = Selector(response).xpath(self.config_items['fd_kd']).extract()
        sig_str = Selector(response).xpath(self.config_items['fd_sig_str']).extract()
        sig_str_percent = Selector(response).xpath(self.config_items['fd_sig_str_pct']).extract()
        total_str = Selector(response).xpath(self.config_items['fd_tot_str']).extract()
        td = Selector(response).xpath(self.config_items['fd_td']).extract()
        td_percent = Selector(response).xpath(self.config_items['fd_td_pct']).extract()
        sub_att = Selector(response).xpath(self.config_items['fd_sub_att']).extract()
        the_pass = Selector(response).xpath(self.config_items['fd_pass']).extract()
        rev = Selector(response).xpath(self.config_items['fd_rev']).extract()
        fd_fight_type = Selector(response).xpath(self.config_items['fd_fight_type']).extract()

        sig_sig_str = Selector(response).xpath(self.config_items['sig_sig_str']).extract()
        sig_sig_str_percent = Selector(response).xpath(self.config_items['sig_sig_str_pct']).extract()
        sig_head = Selector(response).xpath(self.config_items['sig_head']).extract()
        sig_body = Selector(response).xpath(self.config_items['sig_body']).extract()
        sig_leg = Selector(response).xpath(self.config_items['sig_leg']).extract()
        sig_distance = Selector(response).xpath(self.config_items['sig_distance']).extract()
        sig_clinch = Selector(response).xpath(self.config_items['sig_clinch']).extract()
        sig_ground = Selector(response).xpath(self.config_items['sig_ground']).extract()

        if fighters:
            page['fighters'] = fighters
            page['fighter_ids'] = []
        if win_or_lose:
            page['win_or_lose'] = win_or_lose
        if kd:
            page['kd'] = kd
        if sig_str:
            page['sig_str'] = sig_str
        if sig_str_percent:
            page['sig_str_percent'] = sig_str_percent
        if total_str:
            page['total_str'] = total_str
        if td:
            page['td'] = td
        if td_percent:
            page['td_percent'] = td_percent
        if sub_att:
            page['sub_att'] =sub_att
        if the_pass:
            page['the_pass'] = the_pass
        if rev:
            page['rev'] = rev

        if fd_fight_type:
            for img_fight_type in fd_fight_type:
                if img_fight_type:
                    fight_type = performance_bonus_type(img_fight_type)
                    if fight_type == 'belt':
                        page['title_fight'] = 1
                    else:
                        page['performance_bonus'] = 1
                        page['performance_bonus_type'] = fight_type
        else:
            page['title_fight'] = 0
            page['performance_bonus'] = 0
            page['performance_bonus_type'] = ''

        if sig_sig_str:
            page['sig_sig_str'] = sig_sig_str
        if sig_sig_str_percent:
            page['sig_sig_str_percent'] = sig_sig_str_percent
        if sig_head:
            page['sig_head'] = sig_head
        if sig_body:
            page['sig_body'] = sig_body
        if sig_leg:
            page['sig_leg'] = sig_leg
        if sig_distance:
            page['sig_distance'] = sig_distance
        if sig_clinch:
            page['sig_clinch'] = sig_clinch
        if sig_ground:
            page['sig_ground'] = sig_ground

    def _set_fight_stats_per_round_data(self, page, response):
        fighters = Selector(response).xpath(self.config_items['pr_fighters']).extract()
        kd = Selector(response).xpath(self.config_items['pr_kd']).extract()
        sig_str = Selector(response).xpath(self.config_items['pr_sig_str']).extract()
        sig_str_percent = Selector(response).xpath(self.config_items['pr_sig_str_pct']).extract()
        total_str = Selector(response).xpath(self.config_items['pr_tot_str']).extract()
        td = Selector(response).xpath(self.config_items['pr_td']).extract()
        td_percent = Selector(response).xpath(self.config_items['pr_td_pct']).extract()
        sub_att = Selector(response).xpath(self.config_items['pr_sub_att']).extract()
        the_pass = Selector(response).xpath(self.config_items['pr_pass']).extract()
        rev = Selector(response).xpath(self.config_items['pr_rev']).extract()

        sig_sig_str = Selector(response).xpath(self.config_items['pr_sig_sig_str']).extract()
        sig_sig_str_percent = Selector(response).xpath(self.config_items['pr_sig_sig_str_pct']).extract()
        sig_head = Selector(response).xpath(self.config_items['pr_sig_head']).extract()
        sig_body = Selector(response).xpath(self.config_items['pr_sig_body']).extract()
        sig_leg = Selector(response).xpath(self.config_items['pr_sig_leg']).extract()
        sig_distance = Selector(response).xpath(self.config_items['pr_sig_distance']).extract()
        sig_clinch = Selector(response).xpath(self.config_items['pr_sig_clinch']).extract()
        sig_ground = Selector(response).xpath(self.config_items['pr_sig_ground']).extract()

        self.log(fighters)

        if fighters:
            page['pr_fighters'] = fighters
            page['pr_fighter_ids'] = []
        if kd:
            page['pr_kd'] = kd
        if sig_str:
            page['pr_sig_str'] = sig_str
        if sig_str_percent:
            page['pr_sig_str_percent'] = sig_str_percent
        if total_str:
            page['pr_total_str'] = total_str
        if td:
            page['pr_td'] = td
        if td_percent:
            page['pr_td_percent'] = td_percent
        if sub_att:
            page['pr_sub_att'] =sub_att
        if the_pass:
            page['pr_the_pass'] = the_pass
        if rev:
            page['pr_rev'] = rev

        if sig_sig_str:
            page['pr_sig_sig_str'] = sig_sig_str
        if sig_sig_str_percent:
            page['pr_sig_sig_str_percent'] = sig_sig_str_percent
        if sig_head:
            page['pr_sig_head'] = sig_head
        if sig_body:
            page['pr_sig_body'] = sig_body
        if sig_leg:
            page['pr_sig_leg'] = sig_leg
        if sig_distance:
            page['pr_sig_distance'] = sig_distance
        if sig_clinch:
            page['pr_sig_clinch'] = sig_clinch
        if sig_ground:
            page['pr_sig_ground'] = sig_ground

    def _set_new_cookies(self, page, response):
        cookies = []
        for cookie in [x.split(';', 1)[0] for x in response.headers.getlist('Set-Cookie')]:
            if cookie not in self.cookies_seen:
                self.cookies_seen.add(cookie)
                cookies.append(cookie)
        if cookies:
            page['newcookies'] = cookies

