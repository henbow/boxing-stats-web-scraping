from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.http import Request, Response
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor

from the_crawler.items import FightPerRoundDetailAttribute
from the_crawler.settings import SITE_CONFIG_PATH
from the_crawler.libraries.helpers import parse_config, type_parser

class PerRoundBotEngine(Spider):

    name = 'FightMetricsPerRoundEngine'

    def __init__(self, **kw):
        super(PerRoundBotEngine, self).__init__(**kw)

        self.config_path = '%s/fightmetrics.txt' % SITE_CONFIG_PATH
        self.config_items = parse_config(self.config_path)

        self.url = self.config_items['start_url']

        try:
            self.link_extractor = LxmlLinkExtractor(restrict_xpaths=self.config_items['event_crawl_area'], unique=True)
        except KeyError:
            self.link_extractor = LxmlLinkExtractor(unique=True)

        self.cookies_seen = set()

    def start_requests(self):
        return [Request(self.url, callback=self.parse)]

    def parse(self, response):
        page = self._get_item(response)

        r = [page]
        r.extend(self._extract_requests(response))

        return r

    def _extract_requests(self, response):
        r = []
        if isinstance(response, Response):
            links = self.link_extractor.extract_links(response)
            r.extend(Request(x.url, callback=self.parse) for x in links)
        return r

    def _get_item(self, response):
        if isinstance(response, Response):
            type = type_parser(response.url)
            if type != 'event-details' and type != 'fighter-details':
                return self._get_fight_per_round_item(response)

    def _get_fight_per_round_item(self, response):
        self.log("Processing per round fight details data")

        item = FightPerRoundDetailAttribute(
            url=response.url,
            size=str(len(response.body)),
            referer=response.request.headers.get('Referer')
        )

        self._set_fight_stats_per_round_data(item, response)
        self._set_new_cookies(item, response)

        return item

    def _set_fight_stats_per_round_data(self, page, response):
        total_round = Selector(response).xpath(self.config_items['pr_total_round']).extract()
        fighters = Selector(response).xpath(self.config_items['pr_fighters']).extract()
        kd = Selector(response).xpath(self.config_items['pr_kd']).extract()
        sig_str = Selector(response).xpath(self.config_items['pr_sig_str']).extract()
        sig_str_percent = Selector(response).xpath(self.config_items['pr_sig_str_pct']).extract()
        total_str = Selector(response).xpath(self.config_items['pr_tot_str']).extract()
        td = Selector(response).xpath(self.config_items['pr_td']).extract()
        td_percent = Selector(response).xpath(self.config_items['pr_td_pct']).extract()
        sub_att = Selector(response).xpath(self.config_items['pr_sub_att']).extract()
        the_pass = Selector(response).xpath(self.config_items['pr_pass']).extract()
        rev = Selector(response).xpath(self.config_items['pr_rev']).extract()

        sig_sig_str = Selector(response).xpath(self.config_items['pr_sig_sig_str']).extract()
        sig_sig_str_percent = Selector(response).xpath(self.config_items['pr_sig_sig_str_pct']).extract()
        sig_head = Selector(response).xpath(self.config_items['pr_sig_head']).extract()
        sig_body = Selector(response).xpath(self.config_items['pr_sig_body']).extract()
        sig_leg = Selector(response).xpath(self.config_items['pr_sig_leg']).extract()
        sig_distance = Selector(response).xpath(self.config_items['pr_sig_distance']).extract()
        sig_clinch = Selector(response).xpath(self.config_items['pr_sig_clinch']).extract()
        sig_ground = Selector(response).xpath(self.config_items['pr_sig_ground']).extract()

        self.log(fighters)

        if total_round:
            page['pr_total_round'] = " ".join(total_round).strip()
        if fighters:
            page['pr_fighters'] = fighters
            page['pr_fighter_ids'] = []
        if kd:
            page['pr_kd'] = kd
        if sig_str:
            page['pr_sig_str'] = sig_str
        if sig_str_percent:
            page['pr_sig_str_percent'] = sig_str_percent
        if total_str:
            page['pr_total_str'] = total_str
        if td:
            page['pr_td'] = td
        if td_percent:
            page['pr_td_percent'] = td_percent
        if sub_att:
            page['pr_sub_att'] =sub_att
        if the_pass:
            page['pr_the_pass'] = the_pass
        if rev:
            page['pr_rev'] = rev

        if sig_sig_str:
            page['pr_sig_sig_str'] = sig_sig_str
        if sig_sig_str_percent:
            page['pr_sig_sig_str_percent'] = sig_sig_str_percent
        if sig_head:
            page['pr_sig_head'] = sig_head
        if sig_body:
            page['pr_sig_body'] = sig_body
        if sig_leg:
            page['pr_sig_leg'] = sig_leg
        if sig_distance:
            page['pr_sig_distance'] = sig_distance
        if sig_clinch:
            page['pr_sig_clinch'] = sig_clinch
        if sig_ground:
            page['pr_sig_ground'] = sig_ground

    def _set_new_cookies(self, page, response):
        cookies = []
        for cookie in [x.split(';', 1)[0] for x in response.headers.getlist('Set-Cookie')]:
            if cookie not in self.cookies_seen:
                self.cookies_seen.add(cookie)
                cookies.append(cookie)
        if cookies:
            page['newcookies'] = cookies

